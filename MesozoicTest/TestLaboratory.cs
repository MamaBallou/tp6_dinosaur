﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Laboratory;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class TestLaboratory
    {
        [TestMethod]
        public void Test_createDinosaur()
        {
            Dinosaur dinosaur1 = new Dinosaur("Louis", "Stegosaurus", 0);
            Dinosaur dinosaur = Laboratory.Laboratory.createDinosaur("Louis", "Stegosaurus");
            Assert.IsNotNull(Laboratory.Laboratory.createDinosaur("Louis", "Stegosaurus"));
            Assert.AreEqual(dinosaur.name, dinosaur1.name);
            Assert.AreEqual(dinosaur.getSpecie(), dinosaur1.getSpecie());
            Assert.AreEqual(dinosaur.age, dinosaur1.age);
        }
    }
}
