﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mesozoic;
using MesozoicSolution;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("Stegausaurus", Dinosaur.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }

        [TestMethod]
        public void  TestDinosaurGetName()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Assert.AreEqual("Louis", louis.getName());
        }

        [TestMethod]
        public void TestDinosaurGetAge()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurGetSpecie()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Assert.AreEqual("Stegosaurus", louis.getSpecie());
        }
    }
}
