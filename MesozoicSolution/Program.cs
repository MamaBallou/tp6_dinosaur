﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;
using Laboratory;

namespace MesozoicSolution
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dinosaur henry = new Dinosaur("Henry", "Diplodocus", 11);
            Console.WriteLine(henry.sayHello());
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(henry.sayHello());


            Dinosaur theo = Laboratory.Laboratory.createDinosaur("Théo", "T-Rex");
            Console.WriteLine(theo.sayHello());
            Console.WriteLine(henry.hug(theo));
            Console.ReadKey();
        }
    }
}
