﻿using System;
using Mesozoic;

namespace Laboratory
{
    public class Laboratory
    {
        public static Dinosaur createDinosaur(string name, string specie)
        {
            Dinosaur dinosaur = new Dinosaur(name, specie, 0);
            return dinosaur;
        }
    }
}